#!/bin/bash
echo "Start Intalation"
./install-base-tools.sh
./install-git.sh
./install-docker.sh


cd /opt/
git clone https://gitlab.com/workshop-road-to-cloud/container/docker-project/nginx-training-app.git

cd /opt/nginx-training-app/
docker build -t nginx-training-app:final .
docker run -it -d -p 80:80 nginx-training-app:final

echo "End Intalation"